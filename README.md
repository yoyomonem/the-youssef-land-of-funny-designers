# Welcome to [Youssef Land](https://yoyomonem22.wixsite.com/youssefland)

Welcome to [Youssef Land](https://yoyomonem22.wixsite.com/youssefland)! Try clicking on the Youssef Land link right on back of the exclamation mark! You really think Youssef's a girl, right? No, Youssef's **a boy**! You think Youssef's **a boy**!

# How to deactivate or delete your account in [Facebook](https://www.facebook.com)

1. Click on the down arrow icon.
2. Click on Settings & Privacy.
3. Click on Settings.
4. Click on Your Facebook Information.
5. Click on Deactivate Account or Delete Account.
6. Either click on Deactivate account or Delete account.
7. Confirm that you're the one that's deleting or deactivating your [Facebook](https://www.facebook.com) account.

And, voila! Your account has been deactivated or deleted on [Facebook](https://www.facebook.com)!
